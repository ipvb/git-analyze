# Git commit 填充工具

git-complete 是一个简单的 commit 填充工具，根据用户输入的年限区间，git-complete 将填充这些年一整年的提交，而这些提交并无文件修改。本工具仅作测试使用，用其刷高贡献度实际上并无多大意义。
